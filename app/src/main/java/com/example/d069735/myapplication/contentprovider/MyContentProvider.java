package com.example.d069735.myapplication.contentprovider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.d069735.myapplication.db.CourseOfferingsContract;
import com.example.d069735.myapplication.db.CourseOfferingsDbHelper;

/**
 * Created by d069735 on 10.07.17.
 */

public class MyContentProvider extends ContentProvider {

    // database
    private CourseOfferingsDbHelper database;

    private SQLiteDatabase db;

    private static final String AUTHORITY = "com.example.d069735.myapplication.contentprovider";
    private static final String BASE_PATH = "courses"; //TABLE
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH);

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/courses";


    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    public static final int COURSES_TABLE = 1;
    public static final int COURSES_ID = 2;

    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, COURSES_TABLE);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH +"/#", COURSES_ID);

    }

    @Override
    public boolean onCreate() {
         database = new CourseOfferingsDbHelper(getContext());
         db = database.getWritableDatabase();
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(CourseOfferingsContract.CourseOfferingEntry.TABLE_NAME);
        Cursor cursor = null;

        int id = sURIMatcher.match(uri);
        switch (id) {
            case COURSES_TABLE://Get a result based on given time
                cursor = qb.query(database.getReadableDatabase(),projection,selection, selectionArgs ,null, null, sortOrder);
                break;
            case COURSES_ID://Get a result based on given ID
                qb.appendWhere(CourseOfferingsContract.CourseOfferingEntry._ID + "=" + uri.getLastPathSegment());
                cursor = qb.query(database.getReadableDatabase(),projection,selection, selectionArgs,null, null, sortOrder);
                break;
            default:
        }

        /**
         * register to watch a content URI for changes
         */
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
