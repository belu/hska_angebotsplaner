package com.example.d069735.myapplication.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.d069735.myapplication.db.CourseOfferingsContract.CourseOfferingEntry;
import com.example.d069735.myapplication.R;

import java.util.Arrays;
import java.util.List;

/**
 * Created by dominikjohs on 09.07.17.
 */

public class CourseOfferingsDbHelper extends SQLiteOpenHelper {


    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + CourseOfferingsContract.CourseOfferingEntry.TABLE_NAME + " (" +
                    CourseOfferingEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    CourseOfferingEntry.COLUMN_NAME_TITLE + " TEXT," +
                    CourseOfferingEntry.COLUMN_NAME_CATEGORY + " TEXT," +
                    CourseOfferingEntry.COLUMN_NAME_ICON + " INTEGER," +
                    CourseOfferingEntry.COLUMN_NAME_TEACHER + " TEXT," +
                    CourseOfferingEntry.COLUMN_NAME_ROOM + " TEXT," +
                    CourseOfferingEntry.COLUMN_NAME_TIME + " Text)";

    private static final String[] SQL_INSERT_DATA =
                    {"INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") " +" VALUES('Morgenkreis', 'Sonstiges'," + R.mipmap.placeholder + ",'Fuchs', '201', '08:30'); ",
                    "INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") "  +" VALUES('Rechnen', 'Denken'," + R.mipmap.read_write_calc + ",'Schaefer', '301', '09:00'); ",
                    "INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") "  +" VALUES('Mathematik 3', 'Denken'," + R.mipmap.math + ",'Laubenheimer', '201', '09:00'); ",
                    "INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") " + " VALUES('Deutsches Werk', 'Sprachen'," + R.mipmap.german_workshop + ",'Weiss', '303', '09:00'); ",
                    "INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") " +" VALUES('Geschichte', 'Wissenschaften'," + R.mipmap.placeholder + ",'Koerner', '201', '10:00'); ",
                    "INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") " +" VALUES('VWL', 'Sozialwissenschaften'," + R.mipmap.math + ",'Haneke', '303', '11:00'); ",
                    "INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") " +" VALUES('Fussball', 'Sport'," + R.mipmap.german_elementary + ",'Vogelsang', 'Sporthalle', '11:00'); ",
                    "INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") " +" VALUES('Pause', 'Sonstiges'," + R.mipmap.placeholder + ",'', '', '12:00'); ",
                    "INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") " +" VALUES('Theater', 'Kunst/Handwerk'," + R.mipmap.german_workshop + ",'Fuchs', '306', '13:00'); ",
                    "INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") " +" VALUES('Englisch', 'Sprachen'," + R.mipmap.german_workshop + ",'Pape', '205', '13:00'); ",
                    "INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") " +" VALUES('Physik', 'Wissenschaften'," + R.mipmap.german_workshop + ",'Schaefer', '204', '13:00'); ",
                    "INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") " +" VALUES('Kritisches Denken', 'Denken'," + R.mipmap.placeholder + ",'Fuchs', '201', '14:00'); ",
                    "INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") "+" VALUES('Spiele', 'Denken'," + R.mipmap.german_elementary + ",'Broeckel', '202', '14:00'); ",
                    "INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") " +" VALUES('Schach', 'Denken'," + R.mipmap.math + ",'Koerner', '201', '15:00'); ",
                    "INSERT INTO " + CourseOfferingEntry.TABLE_NAME + " ( "+ CourseOfferingEntry.COLUMN_NAME_TITLE + "," + CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," + CourseOfferingEntry.COLUMN_NAME_ICON + "," + CourseOfferingEntry.COLUMN_NAME_TEACHER + "," + CourseOfferingEntry.COLUMN_NAME_ROOM + "," + CourseOfferingEntry.COLUMN_NAME_TIME + ") "+" VALUES('Garten', 'Kunst/Handwerk'," + R.mipmap.placeholder + ",'Ditzinger', 'Garten', '16:00'); "};

    private static final String SQL_DROP_TABLE =
            "DROP TABLE IF EXISTS " + CourseOfferingsContract.CourseOfferingEntry.TABLE_NAME;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Courses.db";

    public CourseOfferingsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public CourseOfferingsDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);

        for(String statement : SQL_INSERT_DATA){
            db.execSQL(statement);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP_TABLE);
        onCreate(db);
    }
}
