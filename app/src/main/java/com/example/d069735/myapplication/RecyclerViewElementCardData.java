package com.example.d069735.myapplication;

import android.graphics.Color;
import android.widget.ImageView;

public class RecyclerViewElementCardData {
    String text;
    int iconNumber;
    int col;
    String room;
    String teacher;
    String category;
    String time;

    public int getIconNumber() {
        return iconNumber;
    }

    public void setIconNumber(int iconNumber) {
        this.iconNumber = iconNumber;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public RecyclerViewElementCardData(){


    }

    public RecyclerViewElementCardData(String text, int iconNumber, int col){
        this.text = text;
        this.iconNumber = iconNumber;
        this.col = col;
    }
    public RecyclerViewElementCardData(String text, int iconNumber, int col, String cat, String teacher, String time, String room){
        this.text = text;
        this.iconNumber = iconNumber;
        this.col = col;
        this.room = room;
        this.time = time;
        this.teacher = teacher;
        this.category = cat;
    }


    public String getText() {
        return text;
    }


    public void setText(String text) {
        this.text = text;
    }


    public int getPic() {
        return iconNumber;
    }


    public void setPic(int iconNumber) {
        this.iconNumber = iconNumber;
    }


    public int getCol() {
        return col;
    }


    public void setCol(int col) {
        this.col = col;
    }


}


