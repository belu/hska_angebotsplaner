package com.example.d069735.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class RecyclerViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //prepareTextviews();
    }

    @Override
    protected void onStart() {
        super.onStart();
        prepareTextviews();
    }

    private void prepareTextviews(){
        TextView category = (TextView) findViewById(R.id.tvCategory);
        TextView room = (TextView) findViewById(R.id.tvRoom);
        TextView teacher = (TextView) findViewById(R.id.tvTeacher);
        TextView time = (TextView) findViewById(R.id.tvTime);
        TextView topic = (TextView) findViewById(R.id.tvTopic);

        TextView categoryText = (TextView) findViewById(R.id.tvCategoryText);
        TextView roomText = (TextView) findViewById(R.id.tvRoomText);
        TextView teacherText = (TextView) findViewById(R.id.tvTeacherText);
        TextView timeText = (TextView) findViewById(R.id.tvTimeText);



        category.setText("Category:");
        categoryText.setText("Placeholder");

        room.setText("Room:");
        roomText.setText("E202");

        teacher.setText("Teacher:");
        teacherText.setText("Prof. Mueller");

        time.setText("Time:");
        timeText.setText("10 am");

        topic.setText("Subject:");


    }


}
