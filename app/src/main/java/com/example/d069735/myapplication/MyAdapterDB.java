package com.example.d069735.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.d069735.myapplication.db.CourseOfferingsContract.CourseOfferingEntry;
import com.example.d069735.myapplication.db.CourseOfferingsDbHelper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by d069735 on 06.07.17.
 */

class MyAdapterDB extends RecyclerView.Adapter<MyAdapterDB.ViewHolder> {

    private SQLiteDatabase mDataset;
    private Context con;
    private Cursor result;

    private final Map<Integer, Cursor> positionToTimeMapping = new HashMap<>();
    private final Map<String, Integer> categoryToColorMapping = new HashMap<>();

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView time;
        public LinearLayout layout;
        public View view;
        public ViewHolder(View v) {
            super(v);
            view = v;

        }
    }
    public static class ViewHolderElement extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public ImageView pic;
        public ViewHolderElement(View v) {
            super(v);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapterDB(/*CourseOfferingsDbHelper myDataset,*/Context con) {
        //mDataset = myDataset.getReadableDatabase();
        this.con = con;

        positionToTimeMapping.put(0, null);//"08:30"
        positionToTimeMapping.put(1, null);//"09:00"
        positionToTimeMapping.put(2, null);//"10:00"
        positionToTimeMapping.put(3, null);//"11:00"
        positionToTimeMapping.put(4, null);//"12:00"
        positionToTimeMapping.put(5, null);//"13:00"
        positionToTimeMapping.put(6, null);//"14:00"
        positionToTimeMapping.put(7, null);//"15:00"
        positionToTimeMapping.put(8, null);//"16:00"

        categoryToColorMapping.put("Sonstiges", Color.GRAY);
        categoryToColorMapping.put("Denken", Color.YELLOW);
        categoryToColorMapping.put("Sprachen", Color.BLUE);
        categoryToColorMapping.put("Wissenschaften", Color.CYAN);
        categoryToColorMapping.put("Sozialwissenschaften", Color.GREEN);
        categoryToColorMapping.put("Sport", Color.MAGENTA);
        categoryToColorMapping.put("Kunst/Handwerk", Color.RED);
        categoryToColorMapping.put("Sprachen", Color.DKGRAY);

    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapterDB.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_text_view, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        vh.layout = (LinearLayout) v.findViewById(R.id.layoutHorizontalCard);
        vh.time = (TextView) v.findViewById(R.id.textView);
        return vh;
    }



    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.mTextView.setText(mDataset[position]);

        try {
            /*
                  result = mDataset.rawQuery("select " +
                    CourseOfferingEntry.COLUMN_NAME_TITLE + "," +
                    CourseOfferingEntry.COLUMN_NAME_ICON + "," +
                    CourseOfferingEntry.COLUMN_NAME_CATEGORY + "," +
                    CourseOfferingEntry.COLUMN_NAME_TIME + "," +
                    CourseOfferingEntry.COLUMN_NAME_ROOM + "," +
                    CourseOfferingEntry.COLUMN_NAME_TEACHER  +
                    " FROM " + CourseOfferingEntry.TABLE_NAME +
                    " where " + CourseOfferingEntry.COLUMN_NAME_TIME + " == '" + positionToTimeMapping.get(position) + "'", null);
            */
            result = positionToTimeMapping.get(position);

                result.moveToFirst();

                holder.time.setText(result.getString(6));
                holder.layout.removeAllViews();
                // CardView cv = (CardView) v.findViewById(R.id.cardView);

                while (!result.isAfterLast()) {
                    View cardLayout = LayoutInflater.from(holder.layout.getContext()).inflate(R.layout.my_card_view, holder.layout, false);
                    CardView card = (CardView) cardLayout.findViewById(R.id.cardView);
                    TextView description = (TextView) cardLayout.findViewById(R.id.recyclerTextView);
                    ImageView icon = (ImageView) cardLayout.findViewById(R.id.imgRecycler);

                    String subject = result.getString(1);
                    int iconCode = result.getInt(3);
                    String category = result.getString(2);
                    String time = result.getString(6);
                    String room = result.getString(5);
                    String teacher = result.getString(4);

                    icon.setImageResource(iconCode);
                    card.setCardBackgroundColor(categoryToColorMapping.get(category));
                    description.setText(subject);
                    setClickEventListenerToCard(card, subject, iconCode, category, time, room, teacher);


                    holder.layout.addView(cardLayout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    result.moveToNext();
                }


        }
        catch (Exception e){
            System.out.println("e"+e.getMessage());
        }
    }
    private void setClickEventListenerToCard(CardView card,
                                             final String subject,
                                             final int iconCode,
                                             final String category,
                                             final String time,
                                             final String room,
                                             final String teacher){
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(v.getContext(),CardDetails.class);
                myIntent.putExtra("room",room);
                myIntent.putExtra("time",time);
                myIntent.putExtra("teacher",teacher);
                myIntent.putExtra("category",category);
                myIntent.putExtra("icon",iconCode);
                myIntent.putExtra("subject",subject);
                try {
                    ((Activity) con).startActivityForResult(myIntent, 0);
                }
                catch(Exception e){
                    System.out.println("e" + e );
                }
            }
        });
    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
       /* Cursor mCount= mDataset.rawQuery("select distinct count(time) from " + CourseOfferingEntry.TABLE_NAME, null);
        mCount.moveToFirst();
        int count = mCount.getInt(0);
        mCount.close();
        */
        int count = positionToTimeMapping.size();
        return count;
    }

    public void addCursor(int id, Cursor c){
        positionToTimeMapping.put(id,c);

    }
}
