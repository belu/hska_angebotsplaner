package com.example.d069735.myapplication;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class CardDetails extends AppCompatActivity {

    String subject ;
    String category ;
    String teacher;
    String room;
    String time;
    int icon;
    int color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //activity_main

        Intent intent = getIntent();
         subject = intent.getStringExtra("subject");
         category = intent.getStringExtra("category");

         teacher  = intent.getStringExtra("teacher");
         room = intent.getStringExtra("room");
         time = intent.getStringExtra("time");
         icon = intent.getIntExtra("icon",0);
        color = intent.getIntExtra("color",0);

        prepareTextviews();
    }


    @Override
    protected void onStart() {
        super.onStart();
    }


private void prepareTextviews(){
        TextView tvCategory = (TextView) findViewById(R.id.tvCategory);
        TextView tvRoom = (TextView) findViewById(R.id.tvRoom);
        TextView tvTeacher = (TextView) findViewById(R.id.tvTeacher);
        TextView tvTime = (TextView) findViewById(R.id.tvTime);
        TextView tvTopic = (TextView) findViewById(R.id.tvTopic);

        TextView categoryText = (TextView) findViewById(R.id.tvCategoryText);
        TextView roomText = (TextView) findViewById(R.id.tvRoomText);
        TextView teacherText = (TextView) findViewById(R.id.tvTeacherText);
        TextView timeText = (TextView) findViewById(R.id.tvTimeText);
        ImageView iv = (ImageView) findViewById(R.id.imageView);

        tvCategory.setText("Category:");
        categoryText.setText(category);

        tvRoom.setText("Room:");
        roomText.setText(room);

        tvTeacher.setText("Teacher:");
        teacherText.setText(teacher);

        tvTime.setText("Time:");
        timeText.setText(time);

        tvTopic.setText(subject);
        iv.setImageResource(icon);


    }


}

