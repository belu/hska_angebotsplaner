package com.example.d069735.myapplication;


import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.app.LoaderManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;


import com.example.d069735.myapplication.db.CourseOfferingsContract;
import com.example.d069735.myapplication.db.CourseOfferingsDbHelper;
import com.example.d069735.myapplication.contentprovider.MyContentProvider;

import java.util.ArrayList;
import java.util.List;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;



public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{


    private RecyclerView mRecyclerView;
    private MyAdapterDB mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private final int LOADER_EIGHT = 0;
    private final int LOADER_NINE = 1;
    private final int LOADER_TEN = 2;
    private final int LOADER_ELEVEN = 3;
    private final int LOADER_TWELVE = 4;
    private final int LOADER_THIRTEEN = 5;
    private final int LOADER_FOURTEEN = 6;
    private final int LOADER_FIFTEEN = 7;
    private final int LOADER_SIXTEEN = 8;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_recycler); //activity_main

        prepareRecyclerView();
        getSupportLoaderManager().initLoader(LOADER_EIGHT, null, this);
        getSupportLoaderManager().initLoader(LOADER_NINE, null, this);
        getSupportLoaderManager().initLoader(LOADER_TEN, null, this);
        getSupportLoaderManager().initLoader(LOADER_ELEVEN, null, this);
        getSupportLoaderManager().initLoader(LOADER_TWELVE, null, this);
        getSupportLoaderManager().initLoader(LOADER_THIRTEEN, null, this);
        getSupportLoaderManager().initLoader(LOADER_FOURTEEN, null, this);
        getSupportLoaderManager().initLoader(LOADER_FIFTEEN, null, this);
        getSupportLoaderManager().initLoader(LOADER_SIXTEEN, null, this);

    }


    @Override
    protected void onStart() {
        super.onStart();
    }


    private void prepareTextviews(){
        TextView category = (TextView) findViewById(R.id.tvCategory);
        TextView room = (TextView) findViewById(R.id.tvRoom);
        TextView teacher = (TextView) findViewById(R.id.tvTeacher);
        TextView time = (TextView) findViewById(R.id.tvTime);
        TextView topic = (TextView) findViewById(R.id.tvTopic);


        TextView categoryText = (TextView) findViewById(R.id.tvCategoryText);
        TextView roomText = (TextView) findViewById(R.id.tvRoomText);
        TextView teacherText = (TextView) findViewById(R.id.tvTeacherText);
        TextView timeText = (TextView) findViewById(R.id.tvTimeText);






        category.setText("Category:");
        categoryText.setText("Placeholder");


        room.setText("Room:");
        roomText.setText("E202");


        teacher.setText("Teacher:");
        teacherText.setText("Prof. Mueller");


        time.setText("Time:");
        timeText.setText("10 am");


        topic.setText("Subject:");




    }
    private void prepareRecyclerView(){
        RecylerViewElement[] myDataset = createData();

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);


// use this setting to improve performance if you know that changes
// in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);


// use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


// specify an adapter (see also next example)
        //mAdapter = new MyAdapter(createData(),getApplicationContext());
        CourseOfferingsDbHelper helper = new CourseOfferingsDbHelper(getApplicationContext());
        mAdapter = new MyAdapterDB(/*helper,*/ this);
        mRecyclerView.setAdapter(mAdapter);


    }

    private RecylerViewElement[] createData(){


        RecylerViewElement[] elements = new RecylerViewElement[9];


        RecylerViewElement element = new RecylerViewElement();
        element.setTime("08:30");
        List<RecyclerViewElementCardData> cards = new ArrayList<>();
        cards.add(new RecyclerViewElementCardData("Morgenkreis", R.mipmap.placeholder, Color.GRAY));
        element.setCards(cards);
        elements[0] = element;

        element = new RecylerViewElement();
        element.setTime("09:00");
        cards = new ArrayList<>();
        cards.add(new RecyclerViewElementCardData("Rechnen", R.mipmap.read_write_calc, Color.GREEN));
        cards.add(new RecyclerViewElementCardData("Mathematik 3", R.mipmap.math, Color.LTGRAY));
        cards.add(new RecyclerViewElementCardData("Deutsches Werk", R.mipmap.german_workshop, Color.GREEN));
        element.setCards(cards);
        elements[1] = element;


        element = new RecylerViewElement();
        element.setTime("10:00");
        cards = new ArrayList<>();
        cards.add(new RecyclerViewElementCardData("Geschichte", R.mipmap.placeholder, Color.RED));
        element.setCards(cards);
        elements[2] = element;


        element = new RecylerViewElement();
        element.setTime("11:00");
        cards = new ArrayList<>();
        cards.add(new RecyclerViewElementCardData("Sozialwissenschaften", R.mipmap.math, Color.YELLOW));
        cards.add(new RecyclerViewElementCardData("Sport", R.mipmap.german_elementary, Color.GRAY));
        element.setCards(cards);
        elements[3] = element;


        element = new RecylerViewElement();
        element.setTime("12:00");
        cards = new ArrayList<>();
        cards.add(new RecyclerViewElementCardData("Pause", R.mipmap.placeholder, Color.YELLOW));
        element.setCards(cards);
        elements[4] = element;


        element = new RecylerViewElement();
        element.setTime("13:00");
        cards = new ArrayList<>();
        cards.add(new RecyclerViewElementCardData("Theater", R.mipmap.german_workshop, Color.BLUE));
        cards.add(new RecyclerViewElementCardData("Sprachen", R.mipmap.german_elementary, Color.GREEN));
        cards.add(new RecyclerViewElementCardData("Wissenschaften", R.mipmap.german_workshop, Color.BLUE));
        element.setCards(cards);
        elements[5] = element;


        element = new RecylerViewElement();
        element.setTime("14:00");
        cards = new ArrayList<>();
        cards.add(new RecyclerViewElementCardData("Kritisches Denken", R.mipmap.placeholder, Color.YELLOW));
        cards.add(new RecyclerViewElementCardData("Spiele", R.mipmap.german_elementary, Color.RED));
        element.setCards(cards);
        elements[6] = element;


        element = new RecylerViewElement();
        element.setTime("15:00");
        cards = new ArrayList<>();
        cards.add(new RecyclerViewElementCardData("Schach", R.mipmap.math, Color.BLUE));
        element.setCards(cards);
        elements[7] = element;


        element = new RecylerViewElement();
        element.setTime("16:00");
        cards = new ArrayList<>();
        cards.add(new RecyclerViewElementCardData("Garten", R.mipmap.placeholder, Color.GREEN));
        element.setCards(cards);
        elements[8] = element;

        return elements;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String selection = CourseOfferingsContract.CourseOfferingEntry.COLUMN_NAME_TIME + " = ?";
        CursorLoader cl = null;
        switch (id){
            case LOADER_EIGHT:
                return new CursorLoader(this, MyContentProvider.CONTENT_URI, null, selection, new String[]{"08:30"}, null);

            case LOADER_NINE:
                return new CursorLoader(this, MyContentProvider.CONTENT_URI, null, selection, new String[]{"09:00"}, null);


            case LOADER_TEN:
                return new CursorLoader(this, MyContentProvider.CONTENT_URI, null, selection, new String[]{"10:00"}, null);

            case LOADER_ELEVEN:
                return new CursorLoader(this, MyContentProvider.CONTENT_URI, null, selection, new String[]{"11:00"}, null);

            case LOADER_TWELVE:
                return  new CursorLoader(this, MyContentProvider.CONTENT_URI, null, selection, new String[]{"12:00"}, null);

            case LOADER_THIRTEEN:
                return new CursorLoader(this, MyContentProvider.CONTENT_URI, null, selection, new String[]{"13:00"}, null);

            case LOADER_FOURTEEN:
                return new CursorLoader(this, MyContentProvider.CONTENT_URI, null, selection, new String[]{"14:00"}, null);

            case LOADER_FIFTEEN:
                return new CursorLoader(this, MyContentProvider.CONTENT_URI, null, selection, new String[]{"15:00"}, null);

            case LOADER_SIXTEEN:
                return new CursorLoader(this, MyContentProvider.CONTENT_URI, null, selection, new String[]{"16:00"}, null);

        }
            throw new IllegalArgumentException("index not found");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
                mAdapter.addCursor(loader.getId(),cursor);
                mAdapter.notifyDataSetChanged();

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}

