package com.example.d069735.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by d069735 on 06.07.17.
 */

class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private RecylerViewElement[] mDataset;
    private Context con;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView time;
        public LinearLayout layout;
        public View view;
        public ViewHolder(View v) {
            super(v);
            view = v;

        }
    }
    public static class ViewHolderElement extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public ImageView pic;
        public ViewHolderElement(View v) {
            super(v);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(RecylerViewElement[] myDataset, Context con) {
        mDataset = myDataset;
        this.con = con;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_text_view, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        vh.layout = (LinearLayout) v.findViewById(R.id.layoutHorizontalCard);
        vh.time = (TextView) v.findViewById(R.id.textView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.mTextView.setText(mDataset[position]);
        RecylerViewElement re = mDataset[position];
        List<RecyclerViewElementCardData> cards =  re.getCards();

        holder.time.setText(re.getTime());
        View v = (View) holder.view;

        holder.layout.removeAllViews();
        // CardView cv = (CardView) v.findViewById(R.id.cardView);

        for(final RecyclerViewElementCardData data : cards )
        {
            View cardLayout = LayoutInflater.from(holder.layout.getContext()).inflate(R.layout.my_card_view, holder.layout, false);
            CardView card = (CardView) cardLayout.findViewById(R.id.cardView);
            TextView description = (TextView) cardLayout.findViewById(R.id.recyclerTextView);
            ImageView icon = (ImageView) cardLayout.findViewById(R.id.imgRecycler);

            icon.setImageResource(data.getPic());
            card.setCardBackgroundColor(data.getCol());
            description.setText(data.getText());
            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent myIntent = new Intent(v.getContext(),CardDetails.class);
                    myIntent.putExtra("room",data.getRoom());
                    myIntent.putExtra("time",data.getTime());
                    myIntent.putExtra("teacher",data.getTeacher());
                    myIntent.putExtra("category",data.getCategory());
                    myIntent.putExtra("icon",data.getPic());
                    myIntent.putExtra("subject",data.getText());
                    myIntent.putExtra("color",data.getCol());
                    try {
                        ((Activity) con).startActivityForResult(myIntent, 0);
                    }
                    catch(Exception e){
                        System.out.println("e" + e );
                    }
                }
            });


            holder.layout.addView(cardLayout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        }
    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
