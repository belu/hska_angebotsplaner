package com.example.d069735.myapplication.db;

import android.provider.BaseColumns;
import android.provider.ContactsContract;

import com.example.d069735.myapplication.R;

/**
 * Created by dominikjohs on 09.07.17.
 */

public class CourseOfferingsContract {


    private CourseOfferingsContract(){

    }

    public static class CourseOfferingEntry implements BaseColumns{

        public static final String TABLE_NAME = "courses";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_CATEGORY = "category";
        public static final String COLUMN_NAME_ICON = "icon";
        public static final String COLUMN_NAME_TEACHER = "teacher";
        public static final String COLUMN_NAME_ROOM = "room";
        public static final String COLUMN_NAME_TIME = "time";



    }

}
